#include "opencv2/opencv.hpp"
#include <iostream>
#include <time.h>

using namespace std;
using namespace cv;

Mat iframe; 
Mat grayframe;
Mat blurframe;
Mat frame;
Mat firstframe;
Mat framedelta;
Mat threshframe;
Mat dilateframe;
Mat oframe;
Mat rects;

vector<vector<Point> > contours;
vector<Vec4i> hierarchy;
vector<vector<Point> > contours_poly( contours.size() );
vector<Rect> boundRect( contours.size() );

int t4 = cv::getTickCount();

int main(){

  // Create a VideoCapture object and use camera to capture the video
  VideoCapture cap(0); 

  // Check if camera opened successfully
  if(!cap.isOpened())
  {
    cout << "Error opening video stream" << endl; 
    return -1; 
  } 

  while(1)
  { 
      
    // Capture frame-by-frame 
    cap >> iframe;
    // If the frame is empty, break immediately
    if (iframe.empty())
      break;
    
    resize(iframe, oframe, Size(500, iframe.rows));
   
    cvtColor(oframe,grayframe,CV_BGR2GRAY);
   
    GaussianBlur(grayframe, grayframe, Size(21, 21), 0);
   
    
    // if the first frame is None, initialize it
    if (firstframe.empty()==1){
      firstframe = grayframe.clone();
      continue;
    }
    absdiff(firstframe, grayframe, framedelta);
    
    threshold(framedelta,threshframe, 25, 255, THRESH_BINARY);
  
    findContours( threshframe.clone(), contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE);
    
    // iterate through each contour.
    for( int i = 0; i< contours.size(); i++ ) 
    { 
     if(contourArea(contours[i],false) < 1000)
            	continue;
     cout << "occupied";    
     
    } 
      
    imshow( "FFrame", firstframe);
    imshow( "Frame", framedelta);
    imshow( "TFrame", threshframe);
    
    // Press  ESC on keyboard to  exit
    char c = (char)waitKey(1);
    if( c == 27 ) 
      break;
  }

  
  int t5 = cv::getTickCount();
  double secs1 = (t5-t4)/cv::getTickFrequency();
  cout << secs1;
  // When everything done, release the video capture and write object
  cap.release();
  
  // Closes all the windows
  destroyAllWindows();
  return 0;
}
